# vagrant-rubygems-mirror

Create a mirror of rubygems.org (to avoid numerous timeouts of host rubygems.org)

## Resources

Software:

* [Gemirro](https://github.com/PierreRambaud/gemirro), GPL-3
* [Rubygems-Mirror](https://github.com/rubygems/rubygems-mirror), MIT
* [Gem In a Box](https://github.com/geminabox/geminabox), MIT

Documentation: 

* StackOverflow [How to build a rubygems mirror server?](https://stackoverflow.com/questions/8411045/how-to-build-a-rubygems-mirror-server/8411230#8411230)
*  [Guides.rubygems.org > Running Gemirro](https://guides.rubygems.org/run-your-own-gem-server/#running-gemirro)